/**
 * 
 */

$(document).ready(
		function() {
			$('#lista-porto').on(
					'change',
					'input[type="checkbox"]',
					function() {
						var $sumchecked = $('#sumchecked-porto');
						var $totalSum = $('#checked-prices-total-sum');
						var totalSumValue = parseInt($totalSum.html());
						var price = parseInt($(this).parent().next().next()
								.html());

						if ($(this).is(':checked')) {
							totalSumValue += price;
						} else {
							totalSumValue -= price;
						}

						$totalSum.html(totalSumValue);
						totalSumValue > 0 ? $sumchecked.css('visibility',
								'visible') : $sumchecked.css('visibility',
								'hidden');
					});
			
			$('#lista-itau').on(
					'change',
					'input[type="checkbox"]',
					function() {
						var $sumchecked = $('#sumchecked-itau');
						var $totalSum = $('#checked-prices-total-sum');
						var totalSumValue = parseInt($totalSum.html());
						var price = parseInt($(this).parent().next().next()
								.html());

						if ($(this).is(':checked')) {
							totalSumValue += price;
						} else {
							totalSumValue -= price;
						}

						$totalSum.html(totalSumValue);
						totalSumValue > 0 ? $sumchecked.css('visibility',
								'visible') : $sumchecked.css('visibility',
								'hidden');
					});
		});