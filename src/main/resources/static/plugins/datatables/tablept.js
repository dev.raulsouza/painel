/**
 * 
 */

$(document).ready(function() {
	$('#table1').DataTable({
		"language" : {
			"sEmptyTable" : "Não foi encontrado nenhum registro",
			"sLoadingRecords" : "A carregar...",
			"sProcessing" : "A processar...",
			"sLengthMenu" : "Mostrar _MENU_ registros",
			"sZeroRecords" : "Não foram encontrados resultados",
			"sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty" : "Mostrando de 0 até 0 de 0 registros",
			"sInfoFiltered" : "(filtrado de _MAX_ registros no total)",
			"sInfoPostFix" : "",
			"sSearch" : "Procurar:",
			"sUrl" : "",
			"oPaginate" : {
				"sFirst" : "Primeiro",
				"sPrevious" : "Anterior",
				"sNext" : "Seguinte",
				"sLast" : "Último"
			},
			"oAria" : {
				"sSortAscending" : ": Ordenar colunas de forma ascendente",
				"sSortDescending" : ": Ordenar colunas de forma descendente"
			}
		}
	});

	$("#example1").DataTable({
		"language" : {
			"sEmptyTable" : "Não foi encontrado nenhum registro",
			"sLoadingRecords" : "A carregar...",
			"sProcessing" : "A processar...",
			"sLengthMenu" : "Mostrar _MENU_ registros",
			"sZeroRecords" : "Não foram encontrados resultados",
			"sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty" : "Mostrando de 0 até 0 de 0 registros",
			"sInfoFiltered" : "(filtrado de _MAX_ registros no total)",
			"sInfoPostFix" : "",
			"sSearch" : "Procurar:",
			"sUrl" : "",
			"oPaginate" : {
				"sFirst" : "Primeiro",
				"sPrevious" : "Anterior",
				"sNext" : "Seguinte",
				"sLast" : "Último"
			},
			"oAria" : {
				"sSortAscending" : ": Ordenar colunas de forma ascendente",
				"sSortDescending" : ": Ordenar colunas de forma descendente"
			}
		},
		"responsive" : true,
		"lengthChange" : true,
		"autoWidth" : false,
		"buttons" : [ {
			extend : 'copy',
			text : 'Copiar'
		}, "excel", 
		{
			extend : 'colvis',
			text : 'Selecionar Colunas'
		}]
	}).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
});