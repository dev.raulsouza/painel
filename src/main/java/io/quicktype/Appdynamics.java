package io.quicktype;

import com.fasterxml.jackson.annotation.*;

public class Appdynamics {
    private Field[] fields;
    private long total;
    private AppdynamicsResult[][] results;
    private boolean moreData;
    private String schema;

    @JsonProperty("fields")
    public Field[] getFields() { return fields; }
    @JsonProperty("fields")
    public void setFields(Field[] value) { this.fields = value; }

    @JsonProperty("total")
    public long getTotal() { return total; }
    @JsonProperty("total")
    public void setTotal(long value) { this.total = value; }

    @JsonProperty("results")
    public AppdynamicsResult[][] getResults() { return results; }
    @JsonProperty("results")
    public void setResults(AppdynamicsResult[][] value) { this.results = value; }

    @JsonProperty("moreData")
    public boolean getMoreData() { return moreData; }
    @JsonProperty("moreData")
    public void setMoreData(boolean value) { this.moreData = value; }

    @JsonProperty("schema")
    public String getSchema() { return schema; }
    @JsonProperty("schema")
    public void setSchema(String value) { this.schema = value; }
}
