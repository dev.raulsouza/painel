package io.quicktype;

import java.io.IOException;
import java.io.IOException;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.*;

@JsonDeserialize(using = AppdynamicsResult.Deserializer.class)
@JsonSerialize(using = AppdynamicsResult.Serializer.class)
public class AppdynamicsResult {
    public ResultResult[] unionArrayValue;
    public String stringValue;

    static class Deserializer extends JsonDeserializer<AppdynamicsResult> {
        @Override
        public AppdynamicsResult deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            AppdynamicsResult value = new AppdynamicsResult();
            switch (jsonParser.currentToken()) {
                case VALUE_STRING:
                    String string = jsonParser.readValueAs(String.class);
                    value.stringValue = string;
                    break;
                case START_ARRAY:
                    value.unionArrayValue = jsonParser.readValueAs(ResultResult[].class);
                    break;
                default: throw new IOException("Cannot deserialize AppdynamicsResult");
            }
            return value;
        }
    }

    static class Serializer extends JsonSerializer<AppdynamicsResult> {
        @Override
        public void serialize(AppdynamicsResult obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            if (obj.unionArrayValue != null) {
                jsonGenerator.writeObject(obj.unionArrayValue);
                return;
            }
            if (obj.stringValue != null) {
                jsonGenerator.writeObject(obj.stringValue);
                return;
            }
            throw new IOException("AppdynamicsResult must not be null");
        }
    }
}
