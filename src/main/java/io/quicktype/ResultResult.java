package io.quicktype;

import java.io.IOException;
import java.io.IOException;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.*;

@JsonDeserialize(using = ResultResult.Deserializer.class)
@JsonSerialize(using = ResultResult.Serializer.class)
public class ResultResult {
    public Long integerValue;
    public String stringValue;

    static class Deserializer extends JsonDeserializer<ResultResult> {
        @Override
        public ResultResult deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            ResultResult value = new ResultResult();
            switch (jsonParser.currentToken()) {
                case VALUE_NUMBER_INT:
                    value.integerValue = jsonParser.readValueAs(Long.class);
                    break;
                case VALUE_STRING:
                    String string = jsonParser.readValueAs(String.class);
                    value.stringValue = string;
                    break;
                default: throw new IOException("Cannot deserialize ResultResult");
            }
            return value;
        }
    }

    static class Serializer extends JsonSerializer<ResultResult> {
        @Override
        public void serialize(ResultResult obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            if (obj.integerValue != null) {
                jsonGenerator.writeObject(obj.integerValue);
                return;
            }
            if (obj.stringValue != null) {
                jsonGenerator.writeObject(obj.stringValue);
                return;
            }
            throw new IOException("ResultResult must not be null");
        }
    }
}
