package br.com.portoseguro.painelsistemas.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ConsultaGA {

	public String consultaGa(String id, String tipoChamada) {
		RestTemplate rest = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		String body = "";

		HttpEntity<String> requestEntity = new HttpEntity<String>(body, headers);
		String url = "http://li2479/gerenciadorArquivoService/api/arquivos?numeroPortoPrint="+ id + "&tipoArquivo=" + tipoChamada;
		ResponseEntity<String> responseEntity = rest.exchange(url, HttpMethod.GET, requestEntity, String.class);
		HttpStatus httpStatus = responseEntity.getStatusCode();
		int status = httpStatus.value();
		String response = responseEntity.getBody();
		System.out.println("Response status: " + status);
		System.out.println(response);
		return response;
	}

}
