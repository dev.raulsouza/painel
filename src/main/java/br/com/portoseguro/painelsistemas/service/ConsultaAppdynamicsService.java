package br.com.portoseguro.painelsistemas.service;

import java.sql.Timestamp;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ConsultaAppdynamicsService {
	
	public String consultaAppdynamics(Timestamp timestampIni, Timestamp timestampFim) {
		RestTemplate rest = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.add("x-events-api-accountname", "customer1_8523277e-e17d-4e16-b78b-e0e2da12f0ee");
		headers.add("x-events-api-key", "f45f6191-48ed-4a25-8783-0aa2e46e4c29");
		headers.add("content-type", "application/vnd.appd.events+json;v=2");

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT eventTimestamp, segments.userData.MC.NumeroPPW, segments.userData.MC.CodigoConvenio, segments.userData.MC.TipoOrcamento FROM transactions WHERE application = \"MULTICALCULO\" and transactionName = \"CalculoSeguroWebService.CalculoSeguroWebPort.calcularOrcamento\" and segments.userData.MC.OrcamentoEmProcessamentoCalculo is not NULL  OR segments.userData.MC.OrcamentoEmProcessamentoPOL IS NOT NULL");
		String body = sb.toString();

		HttpEntity<String> requestEntity = new HttpEntity<String>(body, headers);
		ResponseEntity<String> responseEntity = rest.exchange("http://li3602:9080/events/query?start=" + timestampIni.getTime() + "&end=" + timestampFim.getTime() + "&limit=10000", HttpMethod.POST, requestEntity, String.class);
		HttpStatus httpStatus = responseEntity.getStatusCode();
		int status = httpStatus.value();
		String response = responseEntity.getBody();
//		System.out.println("Response status: " + status);
		System.out.println(response);
		
		return response.substring(1, response.length() - 1);
		

	}

}
