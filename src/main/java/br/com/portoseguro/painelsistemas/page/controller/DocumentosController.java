package br.com.portoseguro.painelsistemas.page.controller;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import br.com.portoseguro.painelsistemas.enums.MarcaEmpresaEnum;
import br.com.portoseguro.painelsistemas.model.Appdynamics;
import br.com.portoseguro.painelsistemas.model.GAResponse;
import br.com.portoseguro.painelsistemas.model.Resultados;
import br.com.portoseguro.painelsistemas.model.ResultsGA;
import br.com.portoseguro.painelsistemas.service.ConsultaAppdynamicsService;
import br.com.portoseguro.painelsistemas.service.ConsultaGA;
import br.com.portoseguro.painelsistemas.util.DateConverter;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/agendamentos")
public class DocumentosController {

	@Autowired
	ConsultaAppdynamicsService cas;

	@Autowired
	ConsultaGA cga;

	public String json = "{\"fields\":[{\"label\":\"eventTimestamp\",\"field\":\"eventTimestamp\",\"type\":\"date\"},{\"label\":\"segments.userData.MC.NumeroPPW\",\"field\":\"segments.userData.MC.NumeroPPW\",\"type\":\"array(integer)\"},{\"label\":\"segments.userData.MC.CodigoConvenio\",\"field\":\"segments.userData.MC.CodigoConvenio\",\"type\":\"array(integer)\"},{\"label\":\"segments.userData.MC.TipoOrcamento\",\"field\":\"segments.userData.MC.TipoOrcamento\",\"type\":\"array(string)\"}],\"total\":10,\"results\":[[\"2021-01-27T13:48:55.459+0000\",[1339743917],[0],[\"SEGURO_NOVO\"]],[\"2021-01-27T13:28:01.559+0000\",[1339739676],[4],[\"RENOVACAO\"]],[\"2021-01-27T13:25:38.865+0000\",[1339740662],[0],[\"SEGURO_NOVO\"]],[\"2021-01-27T13:25:34.717+0000\",[1339740827],[4],[\"SEGURO_NOVO\"]],[\"2021-01-27T13:19:08.723+0000\",[1339736623],[4],[\"SEGURO_NOVO\"]],[\"2021-01-27T13:16:48.486+0000\",[1339735401],[0],[\"RENOVACAO\"]],[\"2021-01-27T13:16:47.679+0000\",[1339734063],[0],[\"SEGURO_NOVO\"]],[\"2021-01-27T13:16:45.909+0000\",[1339735442],[5],[\"SEGURO_NOVO\"]],[\"2021-01-27T12:14:39.924+0000\",[1339654042],[0],[\"SEGURO_NOVO\"]],[\"2021-01-27T03:00:14.241+0000\",[1339700592],[4],[\"SEGURO_NOVO\"]]],\"moreData\":false,\"schema\":\"biz_txn_v1\"}\r\n"
			+ "";

	private static Logger logger = LoggerFactory.getLogger(DocumentosController.class);
	

	@GetMapping(value = "/")
	public ModelAndView getFormularioDocumento() {
		return new ModelAndView("agendamentos/agendamentosform");
	}

	@PostMapping(value = "/")
	public ModelAndView postFormularioDocumento(String datas) {
		ModelAndView mv = new ModelAndView("agendamentos/agendamentos");

		String[] datasPlited = datas.split("-");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

		LocalDateTime strIni = null;
		LocalDateTime strFim;
		try {

			strIni = LocalDateTime.parse((datasPlited[0].trim()), formatter);
			strFim = LocalDateTime.parse((datasPlited[1].trim()), formatter);

			Timestamp timestampIni = Timestamp.valueOf(strIni);
			Timestamp timestampFim = Timestamp.valueOf(strFim);

			System.out.println(timestampIni.getTime());
			System.out.println(timestampFim.getTime());

			cas.consultaAppdynamics(timestampIni, timestampFim);

			String response = cas.consultaAppdynamics(timestampIni, timestampFim);

			List<Resultados> result = retornoResult(response);

			mv.addObject("documentos", result);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return mv;
	}

	private List<Resultados> retornoResult(String response) {

		Gson gson = new Gson();

		Appdynamics f = gson.fromJson(response, Appdynamics.class);

		List<Resultados> resultados = new ArrayList<>();

		System.out.println(f.results.size());

		f.results.forEach(ff -> {
			String norm = ff.toString().replace("[", "").replace("]", "");
			String[] splited = norm.split(",");

			adicionaResultado(splited, resultados);

		});

		return resultados;
	}

	private GAResponse getResponse(String id, String tipoChamada) {

		String response = cga.consultaGa(id, tipoChamada);

		Gson gson = new Gson();

		GAResponse ga = gson.fromJson(response, GAResponse.class);

		return ga;

	}

	private void adicionaResultado(String[] splited, List<Resultados> resultados) {

		String date1 = "N/D";
		String data = "N/D";
		String ppw = "N/D";
		String conv = "N/D";
		String conv1 = "N/D";
		String tipo = "N/D";

		try {
			if (splited[0] != null) {
				date1 = splited[0];
				
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		        format.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
		        Date date = format.parse(date1);
				
				 data = format.format(date).replace("-", "/").replace("T", " ").substring(0, 19);;
				 
				 

			}
		} catch (Exception e3) {
			// TODO Auto-generated catch block
			System.out.println("PROBLEMA DATA" + e3.getMessage());
		}
		try {

			if (splited[1] != null && !splited[1].equals("null") && !splited[1].equals(null)) {

				String value = splited[1];
				BigDecimal result;
				try {
					result = new BigDecimal(value.trim());
					ppw = String.valueOf(result.longValue());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("Problema ao converter:  " + splited[1].trim() + " " + e.getMessage());
				}
			}

		} catch (Exception e2) {
			// TODO Auto-generated catch block
			System.out.println("PROBLEMA PPW" + e2.getMessage());
		}
		try {
			if (splited[2] != null) {
				conv = splited[2].replace(".0", "").trim();
				
				MarcaEmpresaEnum marEmp = MarcaEmpresaEnum.buscarMarca(Integer.valueOf(conv));
				
				conv = marEmp.getDescricao();
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("PROBLEMA CONV" + e1.getMessage());
		}
		try {
			if (splited[3] != null) {
				tipo = splited[3];
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("PROBLEMA tipo" + e.getMessage());
		}

		resultados.add(new Resultados(data, ppw, conv, tipo));

	}

	@GetMapping(value = "/horarios/{id}")
	public ModelAndView postDocumentoAnalise(String ppw, @PathVariable("id") String id) {
		ModelAndView mv = new ModelAndView("documentos/analise");
		GAResponse gar = getResponse(id, "XML_PRECOONLINE_PROPOSTA");
		GAResponse ga1 = getResponse(id, "XML_PRECOONLINE_PROTOCOLO");
		GAResponse ga2 = getResponse(id, "POL_XML_CALCULO_REQUEST");
		GAResponse ga3 = getResponse(id, "POL_XML_CALCULO_RESPONSE");
		GAResponse ga4 = getResponse(id, "XML_CALCULO_PPWEB_PROPOSTA");
		GAResponse ga5 = getResponse(id, "XML_CALCULO_PPWEB_PROTOCOLO");
		GAResponse ga6 = getResponse(id, "XML_MULTICALCULO_SOLICITACAO_ORCAMENTO");
		GAResponse ga7 = getResponse(id, "XML_MULTICALCULO_RESULTADO_ORCAMENTO");
		GAResponse ga8 = getResponse(id, "JSON_MONITORAMENTO_CALCULO");

		List<ResultsGA> results = new ArrayList<>();

		String XML = "N/D";
		String data = (String) "0000000";
		String tipo = "N/D";

		if (!gar.getArquivos().isEmpty()) {
			XML = gar.getArquivos().get(0).getConteudo();
			tipo = gar.getArquivos().get(0).getTipoArquivo();
			data = gar.getArquivos().get(0).getDataCriacao();
			
			data = DateConverter.timeStampToString(data);

			results.add(new ResultsGA(gar.getNumeroPortoPrint().toString(), tipo, data, XML));
		}

		if (!ga1.getArquivos().isEmpty()) {
			XML = ga1.getArquivos().get(0).getConteudo();
			tipo = ga1.getArquivos().get(0).getTipoArquivo();
			data = ga1.getArquivos().get(0).getDataCriacao();
			
			data = DateConverter.timeStampToString(data);

			results.add(new ResultsGA(ga1.getNumeroPortoPrint().toString(), tipo, data, XML));
		}

		if (!ga2.getArquivos().isEmpty()) {
			XML = ga2.getArquivos().get(0).getConteudo();
			tipo = ga2.getArquivos().get(0).getTipoArquivo();
			data = ga2.getArquivos().get(0).getDataCriacao();
			
			data = DateConverter.timeStampToString(data);

			results.add(new ResultsGA(ga2.getNumeroPortoPrint().toString(), tipo, data, XML));
		}

		if (!ga3.getArquivos().isEmpty()) {
			XML = ga3.getArquivos().get(0).getConteudo();
			tipo = ga3.getArquivos().get(0).getTipoArquivo();
			data = ga3.getArquivos().get(0).getDataCriacao();
			
			data = DateConverter.timeStampToString(data);

			results.add(new ResultsGA(ga3.getNumeroPortoPrint().toString(), tipo, data, XML));
		}

		if (!ga4.getArquivos().isEmpty()) {
			XML = ga4.getArquivos().get(0).getConteudo();
			tipo = ga4.getArquivos().get(0).getTipoArquivo();
			data = ga4.getArquivos().get(0).getDataCriacao();
			
			data = DateConverter.timeStampToString(data);

			results.add(new ResultsGA(ga4.getNumeroPortoPrint().toString(), tipo, data, XML));
		}

		if (!ga5.getArquivos().isEmpty()) {
			XML = ga5.getArquivos().get(0).getConteudo();
			tipo = ga5.getArquivos().get(0).getTipoArquivo();
			data = ga5.getArquivos().get(0).getDataCriacao();
			
			data = DateConverter.timeStampToString(data);

			results.add(new ResultsGA(ga5.getNumeroPortoPrint().toString(), tipo, data, XML));
		}

		if (!ga6.getArquivos().isEmpty()) {
			XML = ga6.getArquivos().get(0).getConteudo();
			tipo = ga6.getArquivos().get(0).getTipoArquivo();
			data = ga6.getArquivos().get(0).getDataCriacao();
			
			data = DateConverter.timeStampToString(data);

			results.add(new ResultsGA(ga6.getNumeroPortoPrint().toString(), tipo, data, XML));
		}

		if (!ga7.getArquivos().isEmpty()) {
			XML = ga7.getArquivos().get(0).getConteudo();
			tipo = ga7.getArquivos().get(0).getTipoArquivo();
			data = ga7.getArquivos().get(0).getDataCriacao();
			
			data = DateConverter.timeStampToString(data);

			results.add(new ResultsGA(ga7.getNumeroPortoPrint().toString(), tipo, data, XML));
		}

		if (!ga8.getArquivos().isEmpty()) {
			XML = ga8.getArquivos().get(0).getConteudo();
			tipo = ga8.getArquivos().get(0).getTipoArquivo();
			data = ga8.getArquivos().get(0).getDataCriacao();
			
			data = DateConverter.timeStampToString(data);

			results.add(new ResultsGA(ga8.getNumeroPortoPrint().toString(), tipo, data, XML));
		}

		mv.addObject("documentos", results);

		return mv;
	}

}
