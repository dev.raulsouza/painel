package br.com.portoseguro.painelsistemas.page.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/sobrenos")
public class AmbientesController {
	
	@GetMapping(value = "/")
	public ModelAndView getAmbientes() {
		return new ModelAndView("sobrenos/sobrenos");
	}
	
	@GetMapping(value = "/contatos")
	public ModelAndView getAmbientesAutofrota() {
		return new ModelAndView("sobrenos/contatos");
	}


}
