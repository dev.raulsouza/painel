//package br.com.portoseguro.painelsistemas.model;
//
//import java.util.Date;
//
//public class JSONResponseAppdynamicsModel {
//
//	private Date data;
//	private int[] documento;
//	private int[] convenio;
//	private String[] tipoDoc;
//
//	public Date getData() {
//		return data;
//	}
//
//	public void setData(Date data) {
//		this.data = data;
//	}
//
//	public int[] getDocumento() {
//		return documento;
//	}
//
//	public void setDocumento(int[] documento) {
//		this.documento = documento;
//	}
//
//	public int[] getConvenio() {
//		return convenio;
//	}
//
//	public void setConvenio(int[] convenio) {
//		this.convenio = convenio;
//	}
//
//	public String[] getTipoDoc() {
//		return tipoDoc;
//	}
//
//	public void setTipoDoc(String[] tipoDoc) {
//		this.tipoDoc = tipoDoc;
//	}
//
//	public JSONResponseAppdynamicsModel(Date data, int[] documento, int[] convenio, String[] tipoDoc) {
//		this.data = data;
//		this.documento = documento;
//		this.convenio = convenio;
//		this.tipoDoc = tipoDoc;
//	}
//
//}
