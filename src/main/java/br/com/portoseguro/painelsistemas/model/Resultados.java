package br.com.portoseguro.painelsistemas.model;
import java.util.Date;

import com.google.gson.Gson;

public class Resultados {

	public String data;
	public String ppw;
	public String conv;
	public String tipo;

	public Resultados(String data, String ppw, String conv, String tipo) {
		this.data = data;
		this.ppw = ppw;
		this.conv = conv;
		this.tipo = tipo;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getPpw() {
		return ppw;
	}

	public void setPpw(String ppw) {
		this.ppw = ppw;
	}

	public String getConv() {
		return conv;
	}

	public void setConv(String conv) {
		this.conv = conv;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public String toString() {
		
		return new Gson().toJson(this);
	}

}
