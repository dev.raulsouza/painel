
package br.com.portoseguro.painelsistemas.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Arquivo {

    @SerializedName("tipoArquivo")
    @Expose
    private String tipoArquivo;
    @SerializedName("conteudo")
    @Expose
    private String conteudo;
    @SerializedName("dataCriacao")
    @Expose
    private String dataCriacao;

    public String getTipoArquivo() {
        return tipoArquivo;
    }

    public void setTipoArquivo(String tipoArquivo) {
        this.tipoArquivo = tipoArquivo;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public String getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(String dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

}
