package br.com.portoseguro.painelsistemas.model;

public class ResultsGA {

	public String numeroPortoPrint;
	public String tipoArquivo;
	public String dataCriacao;
	public String conteudo;

	public ResultsGA(String numeroPortoPrint, String tipoArquivo, String dataCriacao, String conteudo ) {
		this.numeroPortoPrint = numeroPortoPrint;
		this.tipoArquivo = tipoArquivo;
		this.dataCriacao = dataCriacao;
		this.conteudo = conteudo;
	}

	public String getNumeroPortoPrint() {
		return numeroPortoPrint;
	}

	public void setNumeroPortoPrint(String numeroPortoPrint) {
		this.numeroPortoPrint = numeroPortoPrint;
	}

	public String getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(String tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public String getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(String dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

}
