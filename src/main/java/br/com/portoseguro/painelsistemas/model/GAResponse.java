
package br.com.portoseguro.painelsistemas.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GAResponse {

    @SerializedName("numeroPortoPrint")
    @Expose
    private Integer numeroPortoPrint;
    @SerializedName("arquivos")
    @Expose
    private List<Arquivo> arquivos = null;

    public Integer getNumeroPortoPrint() {
        return numeroPortoPrint;
    }

    public void setNumeroPortoPrint(Integer numeroPortoPrint) {
        this.numeroPortoPrint = numeroPortoPrint;
    }

    public List<Arquivo> getArquivos() {
        return arquivos;
    }

    public void setArquivos(List<Arquivo> arquivos) {
        this.arquivos = arquivos;
    }

}
