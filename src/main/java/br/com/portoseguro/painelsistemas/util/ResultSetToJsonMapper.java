package br.com.portoseguro.painelsistemas.util;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

public class ResultSetToJsonMapper {

	private ResultSetToJsonMapper() {
		throw new IllegalStateException("Utility class");
	}

	public static JSONArray mapResultSet(ResultSet rs) throws SQLException {
		JSONArray jArray = new JSONArray();
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		do {
			jArray.put(montarObjeto(rsmd, rs, columnCount));
		} while (rs.next());
		return jArray;
	}

	private static JSONObject montarObjeto(ResultSetMetaData rsmd, ResultSet rs, int columnCount) throws SQLException {
		JSONObject jsonObject = new JSONObject();
		for (int index = 1; index <= columnCount; index++) {
			String column = rsmd.getColumnName(index);
			Object value = rs.getString(column);
			if (value == null) {
				jsonObject.put(column, "");
			} else if (value instanceof Integer) {
				jsonObject.put(column, (Integer) value);
			} else if (value instanceof String) {
				jsonObject.put(column, (String) value);
			} else if (value instanceof Boolean) {
				jsonObject.put(column, (Boolean) value);
			} else if (value instanceof Date) {
				jsonObject.put(column, ((Date) value).getTime());
			} else if (value instanceof Long) {
				jsonObject.put(column, (Long) value);
			} else if (value instanceof Double) {
				jsonObject.put(column, (Double) value);
			} else if (value instanceof Float) {
				jsonObject.put(column, (Float) value);
			} else if (value instanceof BigDecimal) {
				jsonObject.put(column, (BigDecimal) value);
			} else if (value instanceof Byte) {
				jsonObject.put(column, (Byte) value);
			} else if (value instanceof byte[]) {
				jsonObject.put(column, (byte[]) value);
			} else {
				throw new IllegalArgumentException("Unmappable object type: " + value.getClass());
			}
		}
		return jsonObject;
	}
}