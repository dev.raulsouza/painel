package br.com.portoseguro.painelsistemas.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.TimeZone;

public class DateConverter {

	private DateConverter() {
		throw new IllegalStateException("Utility class");
	}

	public static String getDataBuscaBanco(String data) throws ParseException {
		Date initDate = new SimpleDateFormat("dd/MM/yyyy").parse(data);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(initDate);
	}

	public static int getSecondsByDate(String data) {
		if (!data.equals("")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

			String inputString = data.substring(0, 12);

			Date date;
			try {
				date = sdf.parse("1970-01-01 " + inputString);
				return (int) date.getTime() / 1000;
			} catch (ParseException e) {
				return 0;
			}
		} else {
			return 0;
		}
	}

	public static String getDataBuscaBancoWithTime(String data) throws ParseException {
		Date initDate = new SimpleDateFormat("dd/MM/yyyy").parse(data);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String[] splited = data.split("\\s+");
		LocalTime pm = LocalTime.of(12, 0);
		LocalTime somaHoras = null;

		if (splited.length == 3) {
			if (splited[2].equals("PM")) {
				String[] timeSplited = splited[1].split(":");
				LocalTime time = LocalTime.of(Integer.valueOf(timeSplited[0]), Integer.valueOf(timeSplited[1]));
				somaHoras = pm.plusHours(time.getHour()).plusMinutes(time.getMinute());
				return formatter.format(initDate) + " " + somaHoras.toString();
			} else {
				return formatter.format(initDate) + " " + splited[1];
			}

		} else {
			return formatter.format(initDate);
		}
	}

	public static String timeStampToString(String data) {
		
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date(Long.parseLong(data));
		System.out.println(sf.format(date));
		
		data = sf.format(date);

		return data;
	}

}
