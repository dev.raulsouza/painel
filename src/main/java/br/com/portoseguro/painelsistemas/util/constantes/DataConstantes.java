package br.com.portoseguro.painelsistemas.util.constantes;

import java.time.format.DateTimeFormatter;

public class DataConstantes {
	
	private DataConstantes() {}
	
	public static final String ISO_DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	public static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String PADRAO_DATA_DDMMYYYY = "dd/MM/yyyy";
	public static final String PADRAO_DATA_YYYYMMDD = "yyyy-MM-dd";
	public static final String PADRAO_DATA_DDMMYYYY_HHMMSS = "dd/MM/yyyy HH:mm:ss";
	public static final String PADRAO_DATA_DDMMYYYY_HHMM = "dd/MM/yyyy HH:mm";
	public static final String FORMATO_YYYYMMDD = "yyyyMMdd";
	public static final String UTC = "UTC";
	
	public static final String COMPLET_DATE_TIME = " 00:00:00";
	public static final String DATETIME_T = "T";
	public static final String TIMEZONE_Z = "Z";
	public static final DateTimeFormatter DATE_FORMATTER_YYYY_MM_DD = DateTimeFormatter.ofPattern(DataConstantes.PADRAO_DATA_YYYYMMDD);
	public static final DateTimeFormatter DATE_FORMATTER_DD_MM_YYYY = DateTimeFormatter.ofPattern(DataConstantes.PADRAO_DATA_DDMMYYYY);
	public static final DateTimeFormatter DATE_FORMATTER_YYYY_MM_DD_T_HH_MM_SS = DateTimeFormatter.ofPattern(DataConstantes.DATE_FORMAT_PATTERN);
	public static final DateTimeFormatter DATE_FORMATTER_DDMMYYYY_HHMMSS = DateTimeFormatter.ofPattern(DataConstantes.PADRAO_DATA_DDMMYYYY_HHMMSS);	
	public static final DateTimeFormatter DATE_FORMATTER_DDMMYYYY_HHMM = DateTimeFormatter.ofPattern(DataConstantes.PADRAO_DATA_DDMMYYYY_HHMM);	
}
