package br.com.portoseguro.painelsistemas.enums;

public enum LversEnum {
	
	LI969("li969", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li969.portoseguro.brasil&from=now-90d&to=now"),
	LI970("li970", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li970.portoseguro.brasil&from=now-90d&to=now"),
	LI1502("li1502", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li1502.portoseguro.brasil&from=now-90d&to=now"),
	LI1503("li1503", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li1503.portoseguro.brasil&from=now-90d&to=now"),
	LI2410("li2410", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2410.portoseguro.brasil&from=now-90d&to=now"),
	LI2413("li2413", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2413.portoseguro.brasil&from=now-90d&to=now");
	
	private String servidor;
	private String memoria;
	private String cpu;
	private String grafana;

	private LversEnum(String servidor, String memoria, String cpu, String grafana) {
		this.servidor = servidor;
		this.memoria = memoria;
		this.cpu = cpu;
		this.grafana = grafana;
	}

	public String getServidor() {
		return servidor;
	}

	public void setServidor(String servidor) {
		this.servidor = servidor;
	}

	public String getMemoria() {
		return memoria;
	}

	public void setMemoria(String memoria) {
		this.memoria = memoria;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getGrafana() {
		return grafana;
	}

	public void setGrafana(String grafana) {
		this.grafana = grafana;
	}

}
