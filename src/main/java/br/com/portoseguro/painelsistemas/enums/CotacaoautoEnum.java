package br.com.portoseguro.painelsistemas.enums;

public enum CotacaoautoEnum {
	
	LI3145("li3145", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3145.portoseguro.brasil&from=now-90d&to=now"),
	LI3146("li3146", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3146.portoseguro.brasil&from=now-90d&to=now"),
	LI3147("li3147", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3147.portoseguro.brasil&from=now-90d&to=now"),
	LI3148("li3148", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3148.portoseguro.brasil&from=now-90d&to=now"),
	LI3149("li3149", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3149.portoseguro.brasil&from=now-90d&to=now"),
	LI3150("li3150", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3150.portoseguro.brasil&from=now-90d&to=now"),
	LI3155("li3155", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3155.portoseguro.brasil&from=now-90d&to=now"),
	LI3156("li3156", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3156.portoseguro.brasil&from=now-90d&to=now"),
	LI3157("li3157", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3157.portoseguro.brasil&from=now-90d&to=now"),
	LI3158("li3158", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3158.portoseguro.brasil&from=now-90d&to=now"),
	LI3159("li3159", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3159.portoseguro.brasil&from=now-90d&to=now"),
	LI3160("li3160", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3160.portoseguro.brasil&from=now-90d&to=now");
	
	
	private String servidor;
	private String memoria;
	private String cpu;
	private String grafana;

	private CotacaoautoEnum(String servidor, String memoria, String cpu, String grafana) {
		this.servidor = servidor;
		this.memoria = memoria;
		this.cpu = cpu;
		this.grafana = grafana;
	}

	public String getServidor() {
		return servidor;
	}

	public void setServidor(String servidor) {
		this.servidor = servidor;
	}

	public String getMemoria() {
		return memoria;
	}

	public void setMemoria(String memoria) {
		this.memoria = memoria;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getGrafana() {
		return grafana;
	}

	public void setGrafana(String grafana) {
		this.grafana = grafana;
	}

}
