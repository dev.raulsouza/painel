package br.com.portoseguro.painelsistemas.enums;

public enum MarcaEmpresaEnum {
	
PORTO_PRINT_WEB(0, "0 - PPWEB"),
PREÇOS_ESTATÍSTICA(1, " 1 - PREÇOS_ESTATÍSTICA"),
PARCERIA_AON(2, "2 - PARCERIA AON"),
VENDA_DIRETA(3, "3 - VENDA DIRETA"),
TELEPORT(4, "4 - TELEPORT"),
SISTEMAS_SEGUROS(5, "5 - SISTEMAS SEGUROS"),
SICS(6, "6 - SICS"),
VENDA_DIRETA_CORRETOR(7, "7 - VENDA DIRETA CORRETOR"),
BANCORBRAS(8, "8 - BANCORBRAS"),
MDSBR(9, "9 - MDSBR"),
MINUTO_SEGUROS(10, "10 - MINUTO SEGUROS"),
KNARUS(11, "11 - KNARUS"),
ADMSEG(12, "12 - ADMSEG"),
CORRETAGEM_FACIL(13, "13 - CORRETAGEM FACIL"),
SICS_DEMAIS(14, "14 - SICS_DEMAIS"),
SuperCalculoSics(15, "15 - SuperCalculoSics"),
ALAVONE(16, "16 - ALAVONE"),
EGESTAO(17, "17 - EGESTAO"),
VENDA_DIRETA_VINCULO(18, "18 - VENDA DIRETA VINCULO"),
COBWAY(19, "19 - COBWAY"),
MULTICANAL(20, "20 - MULTICANAL"),
REFERE(21, "21 - REFERE"),
BIDU(22, "22 - BIDU"),
SEMATIC(23, "23 - SEMATIC"),
SMARTBIT(24, "24 - SMARTBIT"),
SEGFY(26, "26 - SEGFY"),
O_INSURANCE(27, "27 - O_INSURANCE"),
TELEPORT_YAMAHA(30, "30 - TELEPORT-YAMAHA"),
TELEPORT_HONDA(31, "31 - TELEPORT-HONDA"),
SICS_CONGENERE(33, "33 - SICS CONGENERE"),
MOSHE(34, "34 - MOSHE"),
AZUL(35, "35 - AZUL"),
 ICARROS(36, "36 - ICARROS"),
MIGRACAO(40, "40 - MIGRACAO"),
SEGURO_LINK(41, "41 - SEGURO_LINK"),
COAMO(42, "42 - COAMO"),
DATASET(43, "43 - DATASET"),
GRUPO_SGCOR(44, "44 - GRUPO_SGCOR"),
MICROMASSI(45, "45 - MICROMASSI"),
AF_PORTOSEG(46, "46 - AF_PORTOSEG"),
AF_VOLKSVAGEM(47, "47 - AF_VOLKSVAGEM"),
AF_ITAU(48, "48 - AF_ITAU"),
CLARA_ROS(99, "99 - CLARA ROS"),
ITAU(105, "105 - ITAU"),
NOVA_PLATAFORMA(210, "210 - NOVA PLATAFORMA");

	private final Integer codigoMarca;
	private final String descricao;
	
	public Integer getCodigoMarca() {
		return codigoMarca;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	MarcaEmpresaEnum(Integer codigoMarca, String descricao) {
		this.codigoMarca = codigoMarca;
		this.descricao = descricao;
	}
	
	public static MarcaEmpresaEnum buscarMarca(Integer codigoMarca) {
		for (MarcaEmpresaEnum enumeration : values()) {
            if(enumeration.codigoMarca.equals(codigoMarca)) {
                return enumeration;
            }
        }
	
	return null;
	}


}
