package br.com.portoseguro.painelsistemas.enums;

public enum SoaEnum {
	
	LI1035("li1035", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li1035.portoseguro.brasil&from=now-90d&to=now"),
	LI1310("li1310", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li1310.portoseguro.brasil&from=now-90d&to=now"),
	LI1311("li1311", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li1311.portoseguro.brasil&from=now-90d&to=now"),
	LI1461("li1461", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li1461.portoseguro.brasil&from=now-90d&to=now"),
	LI1640("li1640", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li1640.portoseguro.brasil&from=now-90d&to=now"),
	LI1641("li1641", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li1641.portoseguro.brasil&from=now-90d&to=now"),
	LI2150("li2150", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2150.portoseguro.brasil&from=now-90d&to=now"),
	LI2151("li2151", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2151.portoseguro.brasil&from=now-90d&to=now");
	
	private String servidor;
	private String memoria;
	private String cpu;
	private String grafana;

	private SoaEnum(String servidor, String memoria, String cpu, String grafana) {
		this.servidor = servidor;
		this.memoria = memoria;
		this.cpu = cpu;
		this.grafana = grafana;
	}

	public String getServidor() {
		return servidor;
	}

	public void setServidor(String servidor) {
		this.servidor = servidor;
	}

	public String getMemoria() {
		return memoria;
	}

	public void setMemoria(String memoria) {
		this.memoria = memoria;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getGrafana() {
		return grafana;
	}

	public void setGrafana(String grafana) {
		this.grafana = grafana;
	}

}
