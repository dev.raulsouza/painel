package br.com.portoseguro.painelsistemas.enums;

public enum TMGEnum {
	
	LI2479("li2479", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2479.portoseguro.brasil&from=now-90d&to=now"),
	LI2480("li2480", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2480.portoseguro.brasil&from=now-90d&to=now"),
	LI2672("li2672", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2672.portoseguro.brasil&from=now-90d&to=now"),
	LI2740("li2740", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2740.portoseguro.brasil&from=now-90d&to=now"),
	LI2412("li2412", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2412.portoseguro.brasil&from=now-90d&to=now"),
	LI2415("li2415", "30 GB", "8 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2415.portoseguro.brasil&from=now-90d&to=now");
	
	private String servidor;
	private String memoria;
	private String cpu;
	private String grafana;

	private TMGEnum(String servidor, String memoria, String cpu, String grafana) {
		this.servidor = servidor;
		this.memoria = memoria;
		this.cpu = cpu;
		this.grafana = grafana;
	}

	public String getServidor() {
		return servidor;
	}

	public void setServidor(String servidor) {
		this.servidor = servidor;
	}

	public String getMemoria() {
		return memoria;
	}

	public void setMemoria(String memoria) {
		this.memoria = memoria;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getGrafana() {
		return grafana;
	}

	public void setGrafana(String grafana) {
		this.grafana = grafana;
	}

}
