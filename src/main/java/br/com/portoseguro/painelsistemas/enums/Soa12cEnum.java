package br.com.portoseguro.painelsistemas.enums;

public enum Soa12cEnum {
	
	LI2893("li2893", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2893.portoseguro.brasil&from=now-90d&to=now"),
	LI2894("li2894", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2894.portoseguro.brasil&from=now-90d&to=now"),
	LI2895("li2895", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2895.portoseguro.brasil&from=now-90d&to=now"),
	LI2896("li2896", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2896.portoseguro.brasil&from=now-90d&to=now"),
	LI2897("li2897", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2897.portoseguro.brasil&from=now-90d&to=now"),
	LI2898("li2898", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2898.portoseguro.brasil&from=now-90d&to=now"),
	LI2899("li2899", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2899.portoseguro.brasil&from=now-90d&to=now"),
	LI2900("li2900", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2900.portoseguro.brasil&from=now-90d&to=now"),
	LI3533("li3533", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3533.portoseguro.brasil&from=now-90d&to=now"),
	LI3534("li3534", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3534.portoseguro.brasil&from=now-90d&to=now"),
	LI3535("li3535", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3535.portoseguro.brasil&from=now-90d&to=now"),
	LI3536("li3536", "40 GB", "12 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li3536.portoseguro.brasil&from=now-90d&to=now");
	
	private String servidor;
	private String memoria;
	private String cpu;
	private String grafana;

	private Soa12cEnum(String servidor, String memoria, String cpu, String grafana) {
		this.servidor = servidor;
		this.memoria = memoria;
		this.cpu = cpu;
		this.grafana = grafana;
	}

	public String getServidor() {
		return servidor;
	}

	public void setServidor(String servidor) {
		this.servidor = servidor;
	}

	public String getMemoria() {
		return memoria;
	}

	public void setMemoria(String memoria) {
		this.memoria = memoria;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getGrafana() {
		return grafana;
	}

	public void setGrafana(String grafana) {
		this.grafana = grafana;
	}

}
