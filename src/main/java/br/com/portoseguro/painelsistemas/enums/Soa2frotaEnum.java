package br.com.portoseguro.painelsistemas.enums;

public enum Soa2frotaEnum {
	
	LI2947("li2947", "34 GB", "16 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2947.portoseguro.brasil&from=now-90d&to=now"),
	LI2948("li2948", "34 GB", "16 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2948.portoseguro.brasil&from=now-90d&to=now"),
	LI2949("li2949", "34 GB", "16 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2949.portoseguro.brasil&from=now-90d&to=now"),
	LI2957("li2957", "34 GB", "16 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=LI2957.portoseguro.brasil&from=now-90d&to=now"),
	LI2958("li2958", "34 GB", "16 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2958.portoseguro.brasil&from=now-90d&to=now"),
	LI2959("li2959", "34 GB", "16 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2959.portoseguro.brasil&from=now-90d&to=now"),
	LI2950("li2950", "25 GB", "16 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2950.portoseguro.brasil&from=now-90d&to=now"),
	LI2960("li2960", "17 GB", "16 CPUs", "http://grafana/d/000000052/zabbix-servidores-linux?"
			+ "orgId=1&refresh=1m&var-group=Servidores_Linux_-_Prod&var-host=li2960.portoseguro.brasil&from=now-90d&to=now");
	
	private String servidor;
	private String memoria;
	private String cpu;
	private String grafana;

	private Soa2frotaEnum(String servidor, String memoria, String cpu, String grafana) {
		this.servidor = servidor;
		this.memoria = memoria;
		this.cpu = cpu;
		this.grafana = grafana;
	}

	public String getServidor() {
		return servidor;
	}

	public void setServidor(String servidor) {
		this.servidor = servidor;
	}

	public String getMemoria() {
		return memoria;
	}

	public void setMemoria(String memoria) {
		this.memoria = memoria;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getGrafana() {
		return grafana;
	}

	public void setGrafana(String grafana) {
		this.grafana = grafana;
	}

}
